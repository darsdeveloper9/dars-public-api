
## Публичное API проекта [DARS](https://dars.one/)
***

-   [**DarsApi** (инициализация)][1]
    -   [Parameters][2]
    -   [Examples][3]
    -   [**newRandomAccount** ( creator, privateKey )][4]
        -   [Parameters][5]
        -   [Examples][6]
    -   [**sendTokens** ( from, to, quantity, memo, privateKey )][7]
        -   [Parameters][8]
        -   [Examples][9]
    -   [**getBalance** ( account , symbol )][10]
        -   [Parameters][11]
        -   [Examples][12]
    -   [**getСommission** ( cmd )][13]
        -   [Parameters][14]
        -   [Examples][15]
    -   [**checkTransaction** ( block_num, id )][16]
        -   [Parameters][17]
        -   [Examples][18]
    -   [**newPromoCode** ( creator, privateKey )][24]
        -   [Parameters][25]
        -   [Examples][26]
-   [**Дополнительно**][23]


## DarsApi

### Parameters

-   `url` **[string][19]** адресс сети (тестовая или MainNet)

### Examples

```javascript
//Инициализация
//js
const darsApi = require('./DarsApi.js');
testNet=new darsApi("https://blockchain2.dars.one");
//PHP
require 'DarsApi.php';
$testNet = new DarsApi('https://blockchain2.dars.one');
```

### newRandomAccount

Генерация и регистрация аккаунта

Внимание!! Для возможности создавать аккаунты бесплатно вы должны связаться с командой DARS
и подать заявку сообщив имя своего аккаунта в данном случае darsuser1111 (config.blockchain.actor). 
иначе с вашего аккаунта будут списаны токены DRS в эквиваленте 4$ за создании нового аккаунта.

#### Parameters

-   `creator` **[string][19]** аккаунт инициатора транзакции, который платит за создание кошелька
-   `privateKey` **[string][19]** приватный ключ отправителя

#### Examples

```javascript
testNet.newRandomAccount("user11111111","5K8jwHMs3TM4hD9iHs5wXYsuKzx21BczxvRuamqKAMjjcZQTQ5y");
```
Return [Promise][21]<{[Object][22]}>
```javascript
{
 block_num: number,//номер блока в который попала транзакция
 transaction_id: number,
 name: string, //имя созданного аккаунта
 activePrivatKey: string,//Приватный ключ аккаунта для подписи транзакций
 ownerPrivatKey: string,//Приватный ключ владельца аккаунта
 error: any
 }
```

### sendTokens

Отправка токенов

#### Parameters

-   `from` **[string][19]** аккаунт отправителя
-   `to` **[string][19]** аккаунт получателя
-   `quantity` **[string][19]** количество и токенов
-   `memo` **[string][19]** коментарий транзакции
-   `privateKey` **[string][19]** приватный ключ отправителя

#### Examples

```javascript
testNet.sendTokens("user11111111","user2222222222", "100.00000000 DRS", "оплата","5K8jwHMs3TM4hD9iHs5wXYsuKzx21BczxvRuamqKAMjjcZQTQ5y");
```
Return [Promise][21]<{[Object][22]}>
```javascript
{
 block_num: number,
 transaction_id: number,
 from: string,
 to: string,
 quantity: string,
 error: any,
}
```

### getBalance

Получение баланса аккаунта по конкретному токену

#### Parameters

-   `account` **[string][19]** аккаунт по которому запрашиваем баланс
-   `symbol` **[string][19]** символ токена по которому хотим получить баланс

#### Examples

```javascript
testNet.getBalance("user11111111","DRS");
```
Return [Promise][21]<{[Object][22]}>
```javascript
{
 account: string,
 symbol: string,
 balance: any,
 error: any,
}
```

### getСommission

Получение текущей комиссии

Комиссия оплачивается в токенах DRS и её размер 
может менятся при изменении курса DRS.
Оплачивает комиссию инициатор транзакции(from)

#### Parameters

-   `cmd` **[number][20]** номер команды по которой запрашиваем комиссию

#### Examples

```javascript
//20 отправка токенов
//11 создание кошелька для всех кто не является привелигированым 'создателем' 
//40 сервисная команда для компаний(запись в журнал)
testNet.getСommission(20);
```
Return [Promise][21]<{[Object][22]}>
```javascript
{
 exchange_rate: any;
 usd_amount: any;
 drs_commission: any;
 error: any;
}
```
ex.
```javascript
{ exchange_rate: '0.07000000',
  usd_amount: '0.05',
  drs_commission: '0.71428571',
  error: null }
```

### checkTransaction

Проверка статуса транзакции

Транзакция считается принятой если её блок меньше чем номер последнего необратимого блока
и её статус 'executed'

#### Parameters

-   `block_num` **([number][20] \| [string][19])** номер блока транзакции
-   `id` **[string][19]** id транзакции

#### Examples

```javascript
testNet.checkTransaction(1234,"754f4dc837866e821a90eaf73314012cd208ebdb712f28555aad3d9a0e4bf992");
```
Return [Promise][21]<{[Object][22]}>
```javascript
{
 id: string;
 status: string;
 error: any;
}
```
ex.
```javascript
{ id:
  '754f4dc837866e821a90eaf73314012cd208ebdb712f28555aad3d9a0e4bf992',
  status: 'executed',
  error: null }
```
### newPromoCode

Генерация промокода для оплаты регистрации блокчейн аккаунта DARS

с аккаунта creator будут списаны токены DRS в эквиваленте 4$

#### Parameters

-   `creator` **[string][19]** аккаунт инициатора транзакции, который платит за генерацию промокода
-   `privateKey` **[string][19]** приватный ключ отправителя

#### Examples

```javascript
testNet.newPromoCode("user11111111","5K8jwHMs3TM4hD9iHs5wXYsuKzx21BczxvRuamqKAMjjcZQTQ5y");
```
Return [Promise][21]<{[Object][22]}>
```javascript
{
 block_num: number,//номер блока в который попала транзакция
 transaction_id: number,
 promoCode: string, //промокод
 error: any
 }
```


### Дополнительно

#### Получение информации в сети [DARS](https://dars.one/):

Для предотвращения перегрузки сервера время работы метода лимитировано, поэтому при вызове метод отрабатывает по умолчанию 10 мс
и возвращает результат работы и информацию о последнем блоке который успел обработать в полях:
```bash
first_processed_block:{номер первого обработанного блока,таймштамп блока в секундах}
last_processed_block:{номер блока,таймштамп блока в секундах,кол-во найденых транзакций}
```
пример поиска транзакций для вашего аккаунта начиная с 70 блока

```bash
#Для реверсного поиска начиная с последнего доступного блока замените поле "start_index":70 на "reverse":true
curl -s --request POST --url https://blockchain2.dars.one/v1/chain/find_in_blocks --data '{"start_index":70,"code":["dars.invoice"],"action":["user2user"],"wanted":"darsuser1111"}'
```
где:
+ `darsuser1111` имя вашего аккаунта
+ `start_index` - номер блока с которого начинается поиск, если не указан-то будет автоматически

результат будет такого вида
```bash
{"blocks":[{"block":71,"timestamp":"2020-03-19T12:46:30.500","producer":"dars.bp1","confirmed":0,"transactions":[{"status":"executed","ID":"d487d7351b1011c6bdbcc90ed9e487471055c6e79e54cca03e6c92c538795bf7","expiration":"2020-03-19T12:47:00","actions":[{"account":"dars.invoice","name":"user2user","authorization":[{"actor":"darsuser1111","permission":"active"}],"data":{"from":"darsuser1111","to":"darsuser2222","quantity":"1.00 DUSD","memo":"DUSD"}}]}]}],"first_processed_block":[70,1584621990],"last_processed_block":[539,1584622224,1]}
```
и видим что наш запрос успел обработать блоки с 70 по 539 и была найдена 1 транзакция в 71 блоке
```bash
"first_processed_block":[70,1584621990],"last_processed_block":[539,1584622224,1]
```

следовательно следующий запрос можно делать уже с индекса (`start_index`) 540 и так пока last_processed_block < last_irreversible_block_num

в транзакции мы видим перевод 1.00 DUSD аккаунту darsuser2222
```bash
{"from":"darsuser1111","to":"darsuser2222","quantity":"1.00 DUSD","memo":"DUSD"}
```

#### Пример скрипта непрерывного реверсного поиска с глубиной в 3 месяца:
```bash
#!/bin/bash

if [ ! $1 ]; then
    printf " Пожалуйста укажите имя аккаунта\n"
    exit 1
fi

if ! command -v jq >/dev/null 2>&1 ; then
    sudo apt-get install jq -y
fi

# Тестовая сеть
CHAIN="curl -s --request POST --url https://blockchain2.dars.one/v1/chain"

START_INDEX=$($CHAIN/get_info | jq ".last_irreversible_block_num")  #| jq ".head_block_num"


DEEP_TIME=$((86400*90)) #90 суток
CUR_TIME=$(date +%s)
END_TIME=$(($CUR_TIME-$DEEP_TIME))
last_processed_block=()

while [[ $END_TIME -le $CUR_TIME && $START_INDEX -gt 1 ]]
do

REQ=$($CHAIN/find_in_blocks --data '{"start_index":'${START_INDEX}',"code":["dars.invoice"],"action":["user2user"],"wanted":"'$1'","reverse":true}')
last_processed_block=($(echo "$REQ" | jq -c ".last_processed_block[]"))
if [ ${#last_processed_block[*]} -ne 3 ];then
printf "\\nprocessed block from $START_INDEX fail!"
exit 1
fi

START_INDEX=${last_processed_block[0]}
CUR_TIME=${last_processed_block[1]}
if [ ${last_processed_block[2]} -gt 0 ];then
echo $REQ | jq "."
fi

done


exit 0

```

### Получить полностью блок по его номеру
```bash
curl -s --request POST --url https://blockchain2.dars.one/v1/chain/get_block --data '{"block_num_or_id":"71"}'
```

[1]: #darsapi

[2]: #parameters

[3]: #examples

[4]: #newrandomaccount

[5]: #parameters-1

[6]: #examples-1

[7]: #sendtokens

[8]: #parameters-2

[9]: #examples-2

[10]: #getbalance

[11]: #parameters-3

[12]: #examples-3

[13]: #getсommission

[14]: #parameters-4

[15]: #examples-4

[16]: #checktransaction

[17]: #parameters-5

[18]: #examples-5

[19]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String

[20]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number

[21]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise

[22]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object

[23]: #дополнительно

[24]: #newpromocode

[25]: #parameters-6

[26]: #examples-6
