<?php

class DarsApi
{

    /**
     * @var string Адресс сети.
     */
    protected $url;
    

    public function __construct($url)
    {
        $this->url = $url;
    }

    public static function randomName() {
        return substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'), 0, 1)
         . substr(str_shuffle('12345abcdefghijklmnopqrstuvwxyz'), 0, 11);
    }

    public function newRandomAccount($creator,$privateKey){

        $keys_active = json_decode(shell_exec("nohup dars-key 2>&1 &"),true); //Активный ключ для подписи транзакций от лица нового аккаунта
        $keys_owner = json_decode(shell_exec("nohup dars-key 2>&1 &"),true); //Ключ владельца аккаунта для возможности замены активного ключа
        $newName=$this->randomName(); //Рандомное имя аккаунта

        $requestData = "nohup dars-push dars.signup makedrsacc" .
         " '" . '{"from":"' . $creator . '","acc_data":{"field_0":"' .
             $newName . '","field_1":"' . $keys_owner["Public"] . '","field_2":"' 
             . $keys_active["Public"] . '"}}' . "' " . $creator . " " . $privateKey . " " . $this->url . " 2>&1 &";
        
        $result = shell_exec($requestData);
        $response = json_decode($result,true);

        if (is_array($response) && array_key_exists('success', $response) && $response['success'] === true)
        {
            return  (object)[
                "block_num"=>$response["block_num"],
                "transaction_id"=>$response["transaction_id"],
                "name"=>$newName,
                "activePrivatKey"=>$keys_active["Private"],
                "ownerPrivatKey"=>$keys_owner["Private"],
                "error"=>null
            ];
        }
        else
        {
            return  (object)["error"=>$result];
        }
    }

    public function sendTokens($from,$to,$quantity,$memo,$privateKey){
        $requestData = "nohup dars-push dars.invoice user2user" .
         " '" . '{"from":"' . $from . '","to":"' . $to . '","quantity":"' . $quantity . '","memo":"' . $memo . '"}' . "' " 
         . $from . " " . $privateKey . " " . $this->url . " 2>&1 &";
        $result = shell_exec($requestData);
        $response = json_decode($result,true);
        if (is_array($response) && array_key_exists('success', $response) && $response['success'] === true)
        {
            return  (object)[
                "block_num"=>$response["block_num"],
                "transaction_id"=>$response["transaction_id"],
                "from"=>$from,
                "to"=>$to,
                "quantity"=>$quantity,
                "error"=>null
            ];
        }
        else
        {
            return  (object)["error"=>$result];
        }
    }

    public function getBalance(string $actor,string $symbol)
    {

        $requestData = "nohup curl -s --request POST --url " 
        . $this->url . "/v1/chain/get_table_rows --data " .  "'" 
        . '{"code":"dars.token","scope":"' . $actor . '","table":"accounts","json":1}' . "'" . " 2>&1 &";
        $result = shell_exec($requestData);
        $response = json_decode($result,true);

        if (is_array($response) && array_key_exists('rows', $response))
        {
            foreach ($response["rows"] as $row) {
                $pos=strpos($row["balance"], $symbol);
                if($pos){
                    return(object)[
                        "account"=>$actor,
                        "symbol"=>$symbol,
                        "balance"=> explode(' ',$row["balance"])[0],
                        "error"=> null
                    ];
                }
            }
            return (object)[
                "account"=>$actor,
                "symbol"=>$symbol,
                "balance"=> '0',
                "error"=> null
            ];
        }
        return (object)["error"=>$result];
        
    }
    
    public function getСommission($cmd)
    {
        $commission=(object)[
            "exchange_rate"=>null,
            "usd_amount"=>null,
            "drs_commission"=> null,
            "error"=> null
        ];

        $requestData = "nohup curl -s --request POST --url " 
        . $this->url . "/v1/chain/get_table_rows --data " .  "'" 
        . '{"code":"dars.invoice","scope":"dars.invoice","table":"exchrate","json":1}' . "'" . " 2>&1 &";
        $result = shell_exec($requestData);
        $response = json_decode($result,true);

        if (is_array($response) && array_key_exists('rows', $response))
        {
            $commission->exchange_rate=round(($response["rows"][0])["rate"],8,PHP_ROUND_HALF_UP);
            $requestData = "nohup curl -s --request POST --url " 
            . $this->url . "/v1/chain/get_table_rows --data " .  "'" 
            . '{"code":"dars.invoice","scope":"dars.invoice","table":"feeusd","key_type": "i64","lower_bound":' . $cmd . ',"upper_bound":' . $cmd . ',"limit":1,"json":1}' 
            . "'" . " 2>&1 &"; 
            $result = shell_exec($requestData);
            $response = json_decode($result,true);
            if (is_array($response) && array_key_exists('rows', $response)){
                $commission->usd_amount=round(($response["rows"][0])["fee"],2,PHP_ROUND_HALF_UP);
            }else{
                return (object)["error"=>$result];
            }
            $commission->drs_commission=round($commission->usd_amount / $commission->exchange_rate,8,PHP_ROUND_HALF_UP);
            return $commission;

        }
        return (object)["error"=>$result];
        
    }
    
    public function checkTransaction($block_num,$id) {
        $LI=(object)[
          "id"=>$id,
          "status"=>"waiting",
          "error"=>null
        ];  
        
        $requestData = "nohup curl -s --request POST --url " 
        . $this->url . "/v1/chain/get_info" . " 2>&1 &";
        $result = shell_exec($requestData);
        $response = json_decode($result,true);
        if (is_array($response) && array_key_exists('last_irreversible_block_num', $response)){
            if($response['last_irreversible_block_num']>$block_num){
                $requestData = "nohup curl -s --request POST --url " 
                . $this->url . "/v1/chain/get_block --data " .  "'" 
                . '{"block_num_or_id":' . $block_num . '"json":1}' . "'" ." 2>&1 &";
                $result = shell_exec($requestData);
                $response = json_decode($result,true);
                if (is_array($response) && array_key_exists('transactions', $response)){ 
                    foreach ($response["transactions"] as $transaction) {
                        if($transaction["trx"]["id"]==$id){
                            $LI->status=$transaction["status"];
                            return $LI;
                        }
                    }
                    $LI->error="Transaction not found";                   
                }else{
                    return (object)["error"=>$result];
                }
            }
            return $LI;
        }

        return (object)["error"=>$result];      
    }    

    public function newPromoCode($creator,$privateKey){

        $promoCode=substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789'), 0, 24);
        $promohash=hash('sha256',$promoCode);
        $requestData = "nohup dars-push dars.signup createpromo" .
         " '" . '{"creator":"' . $creator . '","hash":"' . $promohash . '"}' . "' " . $creator . " " . $privateKey . " " . $this->url . " 2>&1 &";
        
        $result = shell_exec($requestData);
        $response = json_decode($result,true);

        if (is_array($response) && array_key_exists('success', $response) && $response['success'] === true)
        {
            return  (object)[
                "block_num"=>$response["block_num"],
                "transaction_id"=>$response["transaction_id"],
                "promoCode"=>$promoCode,
                "error"=>null
            ];
        }
        else
        {
            return  (object)["error"=>$result];
        }
    }    

}
